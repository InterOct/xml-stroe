package com.example.xmlstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XmlStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(XmlStoreApplication.class, args);
	}
}
