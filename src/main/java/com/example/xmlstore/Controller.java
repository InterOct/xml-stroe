package com.example.xmlstore;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;

@RestController
public class Controller {

    private final ApplicationContext applicationContext;

    @Autowired public Controller(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/")
    @SneakyThrows(IOException.class)
    public ResponseEntity doPost(@RequestBody String body) throws SAXException {
        InputStream schemaStream = applicationContext.getResource("classpath:xsd/food.xsd").getInputStream();
        if (XMLValidator.validate(new StreamSource(new StringReader(body)), new StreamSource(schemaStream))) {
            try (PrintWriter pw = new PrintWriter("xml.xml")) {
                pw.print(body);
            }
        }
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @ExceptionHandler(SAXException.class)
    public ResponseEntity<String> handleException(SAXException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
